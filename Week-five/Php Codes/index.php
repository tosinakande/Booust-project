<?php

session_start();
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>FILM RENTAL SYSTEM</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="css/index.css" />
    
    
</head>
<body>
       
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <a class="navbar-brand" href="/">
                    <img src="logo.png">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"></span>
                </button>
              
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                  <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                      <a class="nav-link" href="#">HOME <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="gallery.php">GALLERY</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="about.php">ABOUT US</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="contact.php">CONTACT</a>
                    </li>
                  </ul>
                  <form class="form-inline my-2 my-lg-0">
                    <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                        <?php if (isset($_SESSION['name']) ): ?>
                            <button class="btn btn-outline-success my-2 my-sm-0" type="login"><a href="login.php">Log Out</a></button>
                        <?php else: ?>
                            <button class="btn btn-outline-success my-2 my-sm-0" type="login"><a href="login.php">Log In</a></button>
                        <?php endif; ?>
                  </form>
                </div>
              </nav>  
            
 <!--  <header class="header">
        <h1><img src="logo.png"></h1>
        <nav>
            <ul>
                <li class="active"><a href="#">HOME</a></li>
                <li><a href="#">MEDIA</a></li>
                <li><a href="#">CONTACT</a></li>
                <li><a href="#">ABOUT US</a></li>
                <li><input type="text" placeholder="Search for Movie.."></li>
            </ul>
        </nav>
    </header> -->

   <div class="container"> 
        <div class="jumbotron">
            <h3 class="display-4">MOVIE REVIEWS</h3>
            <p class="lead">Film rental services does movie reviews of currently released movies and also provides link for download. Browse
                through our collection and enjoy.</p>
        </div>
        
            <h1>ACRIMONY</h1>
            <img src="TylerPerrysAcrimonyTeaserPoster.jpg">
            <P>
                Acrimony is a 2018 American psycological triler film produced, written and directed by <strong>Tyler Perry</strong>.
                The film stars <strong>Taraji P. Henson, Lyriq Bent, Jazmyn Simon, and Crystle Stewart</strong>, and 
                follows a loyal wife who decides to take revenge on her ex-husband. Click <a href="#">here</a> to download.
            </P>
            <h1>A Quiet Place</h1>
            <img src="A_Quiet_Place_film_poster.png">
            <p>
                A quiet place is a 2018 American horror film directed by <strong>John Krasinski</strong>, who stars with <strong>
                Emily Blunt</strong>, his real-life wife. In Quiet Place, a family must live in silence while hiding from creatures
                which hunt by sound. Click <a href="#">here</a> to download.
            </p>
            <h1>Avengers: Infinity War</h1>
            <img src="Avengers_Infinity_War_poster.jpg">
            <p>
                Avengers: Infinity War is a 2018 American superhero film based on the Marvel Comics superhero team the Avengers, produced
                by Marvel Studios. it is the sequel to 2012's The Avengers and 2015's Avengers: Age of Ultron. in the film, the avengers
                and the Guardians of the Galaxy attempt to stop Thanos from ammasing the all-powerful Infinity stones. Click <a href="#">here</a> 
                to download. 
            </p>
            
                <a href="#">&laquo;</a>
                <a href="#">1</a>
                <a href="#">2</a>
                <a href="#">3</a>
                <a href="#">4</a>
                <a href="#">5</a>
                <a href="#">6</a>
                <a href="#">&raquo;</a>
    </div>

    <footer class "footer">
            <p>Copyright &copy 2018 FILM RENTALSERVICES</p>
    </footer>

    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/bootstrap.js"></script>

</body>
</html>