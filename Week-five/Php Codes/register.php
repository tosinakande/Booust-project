<?php
    session_start();

    include 'database.php';

        if (isset($_POST['name']) && isset($_POST['email']) && isset($_POST['password']) ){
            try{
                
                $query = "INSERT INTO users SET name=:name, email=:email, password=:password, created_at=:created_at";
                $stmt = $connection->prepare($query);

                $name = $_POST['name'];
                $email = $_POST['email'];
                $password = $_POST['password'];

                $stmt->bindParam(':name', $name);
                $stmt->bindParam(':email', $email);
                $stmt->bindParam(':password', hash('sha256', $password));
                $stmt->bindParam(':created_at', date('Y-m-d H:i:s'));


                if ($stmt->execute()){
                    
                    $_SESSION['name'] = $name;
                    header("Location: index.php");
                    exit();

                }else{
                    echo "<div class='alert alert-danger'>Unable to register at this time.</div>";
                }

            }
            catch(PDOException $exception){
                die('ERROR: ' . $exception->getMessage());
            }
        }

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>FILM RENTAL SYSTEM</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="css/index.css" />

</head>
<body>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <a class="navbar-brand" href="/">
                    <img src="logo.png">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"></span>
                </button>
              
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                  <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                      <a class="nav-link" href="index.php">HOME</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="gallery.php">GALLERY</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#">ABOUT US</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="contact.php">CONTACT</a>
                    </li>
                  </ul>
                  <form class="form-inline my-2 my-lg-0">
                    <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                  </form>
                </div>
              </nav>  

             <div class="container">
                <h1 align="center">Register</h1>

                <form action="register.php" method="POST">
                    <div class="form-group">
                        <label for="name">NAME</label>
                        <input type="text" class="form-control" id="name" name="name" required="required">
                    </div>
                    <div class="form-group">
                        <label for="email">EMAIL</label>
                        <input type="email" class="form-control" id="email" name="email" required="required">
                    </div>
                    <div class="form-group">
                        <label for="password">PASSWORD</label>
                        <input type="password" class="form-control" id="password" name="password" required="required">
                    </div>
                   <input type="submit" name="btnLogin" class="btn btn-primary" value="Register">
                </form>
                <br>
                <P>Have an acount <a href="login.php" style="color:red";>Login</a></p>

              </div>

          
        <footer class "footer">
                    <p>Copyright &copy 2018 FILM RENTALSERVICES</p>
        </footer>
        
       
</body>
</html>