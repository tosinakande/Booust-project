<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>FILM RENTAL SYSTEM</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="css/index.css" />

</head>
<body>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <a class="navbar-brand" href="/">
                    <img src="logo.png">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"></span>
                </button>
              
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                  <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                      <a class="nav-link" href="index.php">HOME</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="gallery.php">GALLERY</a>
                    </li>
                    <li class="nav-item active">
                      <a class="nav-link" href="#">ABOUT US <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="contact.php">CONTACT</a>
                    </li>
                  </ul>
                  <form class="form-inline my-2 my-lg-0">
                    <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                    <?php if (isset($_SESSION['name']) ): ?>
                    <button class="btn btn-outline-success my-2 my-sm-0" type="login"><a href="login.php">Log Out</a></button>
                    <?php else: ?>
                    <button class="btn btn-outline-success my-2 my-sm-0" type="login"><a href="login.php">Log In</a></button>
                    <?php endif; ?>
                  </form>
                </div>
              </nav>  

             <div class="container">
              <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                      <li class="breadcrumb-item active" aria-current="page">About Us</li>
                    </ol>
                </nav>

                <h1>
                    About Us
                </h1>
                <p>
                    Film Renatal Services was birthed with the initiave to give people access to content in which is not readily available.
                    Our reviews are carefully and thoughfully done by seasoned experts who pride themselves to having the required knowledge and
                    industry insight to be able to effectively transcribe actual happenings and give meaningful intrepreation as deemed fit to
                    every work been reviewed.
                </p>

                <h1>Our Mission</h1>
                <p>To get to the top of the review chain and as such be the go to site for every movie lover.</p>

                <h1>Our Vission</h1>
                <p>
                    Providing the best and well thought out content by paying attention to every little detail for our clients
                </p>
                </div>

        <footer class "footer">
                    <p>Copyright &copy 2018 FILM RENTALSERVICES</p>
        </footer>
        
        <script src="js/jquery-3.3.1.min.js"></script>
        <script src="js/bootstrap.js"></script>
    
</body>
</html>