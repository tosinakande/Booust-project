<?php
                    session_start();

                    include 'database.php';

                    if (isset($_POST['email']) && isset($_POST['password']) ){
                        try{
                            $query = "SELECT name FROM users WHERE email=:email AND password=:password";
                            $stmt = $connection->prepare($query);

                            $email = $_POST['email'];
                            $password = hash('sha256', $_POST['password']);

                            $stmt->bindParam(':email', $email);
                            $stmt->bindParam(':password', $password);
                            $stmt->execute();

                            $num = $stmt->rowCount();

                                if($num>0){
                                    $row = $stmt->fetch();
                                    $_SESSION['name'] = $row['name'];
                                    header("location: index.php");
                                    exit();
                                } else {
                                   $message = 'Invalid username/password combination.';
                                }
                        }

                        catch(PDOException $exception){
                            die('ERROR: ' . $exception->getMessage());
                        }
                    }

                    else {
                        session_destroy();
                    }

                ?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>FILM RENTAL SYSTEM</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="css/index.css" />

</head>
<body>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <a class="navbar-brand" href="/">
                    <img src="logo.png">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"></span>
                </button>
              
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                  <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                      <a class="nav-link" href="index.php">HOME</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="gallery.php">GALLERY</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#">ABOUT US</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="contact.php">CONTACT</a>
                    </li>
                  </ul>
                  <form class="form-inline my-2 my-lg-0">
                    <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                  </form>
                </div>
              </nav>  

             <div class="container">
                <h1 align="center">Log In</h1>

                <form action="login.php" method="POST">
                    <div class="form-group">
                        <label for="email">EMAIL</label>
                        <input type="email" class="form-control" id="email" name="email" required="required">
                    </div>
                    <div class="form-group">
                        <label for="password">PASSWORD</label>
                        <input type="password" class="form-control" id="password" name="password" required="required">
                    </div>
                   <input type="submit" name="btnLogin" class="btn btn-primary" value="Login">
                </form>
                <br>
                <P>Don't have an acount yet? <a href="register.php" style="color:red";>Click here to Register</a></p>

                <?php if (isset($message)) { echo $message; } ?>

              </div>

          
        <footer class "footer">
                    <p>Copyright &copy 2018 FILM RENTALSERVICES</p>
        </footer>
        
       
</body>
</html>