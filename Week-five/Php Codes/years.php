<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>PHP Fundamantals</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="bootstrap.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="#">PHPfundamental</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

 <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="index.html">Movie Rental</a>
      </li>
      <li class="nav-item">
        <a class="nav-link disabled" href="#">Disabled</a>
      </li>
      <li class="nav-item">
        <a class="nav-link disabled" href="#"></a>
      </li>
    </ul> 
    <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
    </form>
  </div>
</nav>

<div class="jumbotron jumbotron-fluid">
  <div class="container">
    <h1 class="display-6">A simple Program</h1>
    <p class="lead">This program would echo out years between 1980 and 2018, identifying leap years and print out the total number of leap years.</p>
  </div>
</div>

<div class="content">
 
  <h1>Program</h1>
  
  <?php 
  $timestamp = strtotime('1980');
  $startyear = date('Y', $timestamp);

  $timestamp = strtotime('2018');
  $stopyear = date('Y', $timestamp);

  $counter = 0;

  while ($startyear <= $stopyear) {

    if (leapyear($startyear)) {
      $counter = $counter + 1;
      echo $startyear . ' ' . 'Leap Year' . '<br>';
    }
    else
    echo $startyear . '<br>';

    $startyear = $startyear + 1;
  }

  echo '<br>' . 'Total number of leap years =' . $counter; 




  function leapyear($startyear) {
  
    $leapyear = false;
    if ( (($startyear % 4) == 0 && ($startyear % 100) != 0) || ($startyear % 400) == 0)
    $leapyear = true;
    return $leapyear;
  }
  ?>

</div>

</body>
</html>