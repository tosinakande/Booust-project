<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>FILM RENTAL SYSTEM</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="css/index.css" />

</head>
<body>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <a class="navbar-brand" href="/">
                    <img src="logo.png">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"></span>
                </button>
              
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                  <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                      <a class="nav-link" href="index.php">HOME</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="gallery.php">GALLERY</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="about.php">ABOUT US</a>
                    </li>
                    <li class="nav-item active">
                      <a class="nav-link" href="#">CONTACT <span class="sr-only">(current)</span></a>
                    </li>
                  </ul>
                  <form class="form-inline my-2 my-lg-0">
                    <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                        <?php if (isset($_SESSION['name']) ): ?>
                            <button class="btn btn-outline-success my-2 my-sm-0" type="login"><a href="login.php">Log Out</a></button>
                        <?php else: ?>
                            <button class="btn btn-outline-success my-2 my-sm-0" type="login"><a href="login.php">Log In</a></button>
                        <?php endif; ?>
                  </form>
                </div>
              </nav>  

              <div class="container">
                    <nav aria-label="breadcrumb">
                          <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Contact</li>
                          </ol>
                      </nav>

               <?php if ( isset($_POST['name']) && isset($_POST['email']) && isset($_POST['message']) ): ?>

                  <?php
                    include "contactus.php";

                    contactus::process($_POST);
                  ?>

                    <h3>Thank you for contacting us.</h3>
                    <h3>An Email has been sent to you</h3>
                    <h3>Click <a href="contact.php">here</a> to go back to the form</h3>

                  <?php else: ?> 

                  <form action="contact.php" method="POST">

                  <div class="form-group">
                      <label for="name">NAME</label>
                      <input type="text" class="form-control" id="name" name="name" placeholder="Enter Name" required="required">
                    </div>
                  <div class="form-group">
                      <label for="email">EMAIL</label>
                        <input type="email" class="form-control" id="email" name="email" placeholder="Enter email" required="required">
                     </div>
                    <div class="form-group">
                        <label for="message">MESSAGE</label>
                        <textarea class="form-control" id="message" name="message" rows="3" required="required"></textarea>
                      </div>
                    
                      <input type="submit" value="submit">
                    </form>

                 <!-- <form action="contact.php" method="POST">
                    NAME: <input type="text" id="name" name="name" required="required"><br><br>

                     EMAIL: <input type="email" id="email" name="email" required="required"><br><br>

                     MESSAGE: <textarea id="message" name="message" required="required"></textarea><br><br>

                     <input type="submit" value="submit">
                     </form> -->

                  <?php endif; ?>

              </div>

        <footer class "footer">
                        <p>Copyright &copy 2018 FILM RENTALSERVICES</p>
        </footer>
            
        <script src="js/jquery-3.3.1.min.js"></script>
        <script src="js/bootstrap.js"></script>
    
</body>
</html>